#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例050：随机数

题目 输出一个随机数。

程序分析 使用 random 模块。
"""
import random


def main():
    print(random.random())
    print(random.uniform(10, 20))
    print(random.randint(10, 20))


if __name__ == "__main__":
    main()
