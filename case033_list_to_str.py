#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例033：列表转字符串

题目 按逗号分隔列表。

程序分析 无。
"""


def main():
    a = [1, 2, 3, 4, 5, 6]
    # join函数
    s = ''
    for i in a:
        s += str(i)
    print(s)
    print(','.join(str(n) for n in a))
    # s = (str(n) for n in a)    # 生成器
    s = [str(n) for n in a]      # 列表  区别是() 和[]
    print(s)
    # print(next(s))


if __name__ == "__main__":
    main()
