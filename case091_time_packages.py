#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例091：time模块

题目 时间函数举例1。

程序分析 无。
"""
import time


def main():
    print(time.time())
    print(time.ctime(time.time()))
    print(time.asctime(time.localtime(time.time())))
    print(time.asctime(time.gmtime(time.time())))
    print(time.strftime('%Y-%m-%d %H:%M:%S'))
    print(time.localtime(time.time()))
    print(time.strftime('%a %A %b %p', time.localtime(time.time())))

    print(time.strftime('a% A% %Y-%m-%d %H:%M:%S', time.localtime(time.time())))
    print(time.strftime('%j', time.localtime(time.time())))
    print(time.strftime('%j'))
    print(time.strptime('2020-03-19', '%Y-%m-%d'))
    print(tuple(time.strptime('2020-03-19', '%Y-%m-%d')))
    print(time.strftime('%j', time.strptime('2020-03-19', '%Y-%m-%d')))


if __name__ == "__main__":
    main()
