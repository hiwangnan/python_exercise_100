#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例026：递归求阶乘

题目 利用递归方法求5!。

程序分析 递归调用即可。
"""
lis = []


def func(n):
    lis.append(n)
    if n > 1:
        x = func(n - 1)
        return n * x
    else:
        return 1


def main():

    print(func(5))
    print(lis)


if __name__ == "__main__":
    main()
