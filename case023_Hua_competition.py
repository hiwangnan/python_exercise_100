#! /usr/bin/python3
# _*_ conding:utf-8 _*_
"""
实例023：画菱形

题目 打印出如下图案（菱形）:

        *
       ***
      *****
     *******
      *****
       ***
        *

"""


def draw(num):
    a = "*" * (2 * (4 - num) + 1)
    print(a.center(9, ' '))
    if num != 1:
        draw(num - 1)
        print(a.center(9, ' '))


def main():
    draw(4)


if __name__ == "__main__":
    main()
