#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例030：回文数

题目 一个5位数，判断它是不是回文数。即12321是回文数，个位与万位相同，十位与千位相同。

程序分析 用字符串比较方便,就算输入的不是数字都ok。
"""


def main():
    st_in = input('请输入一个数字')
    is_hui = True
    for i in range(int(len(st_in)/2)):
        if st_in[i] != st_in[-(i+1)]:
            is_hui = False
            break
    if is_hui:
        print(st_in, '是回文数')
    else:
        print(st_in, '不是回文数')


if __name__ == "__main__":
    main()
