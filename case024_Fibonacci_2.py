#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
题目 有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13…求出这个数列的前20项之和。

程序分析 就是斐波那契数列的后一项除以前一项。1,2,3,5,8,
"""


def main():
    x = 1
    y = 1
    lis = []
    li = []
    for i in range(10):
        lis.append(y)
        x = (x + y)
        li.append(x / y)
        lis.append(x)
        y = x + y
        li.append(y / x)

    print(lis)
    print(li)
    print(sum(li))


if __name__ == "__main__":
    main()
