#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例048：数字比大小

题目 数字比较。

程序分析 无
"""


def main():
    a = int(input('请输入第一个数字：'))
    b = int(input('请输入第二个数字：'))
    print('输入的数字为:(a=%d,b=%d)' % (a, b))
    if a > b:
        print('a>b')
    elif a < b:
        print('a<b')
    else:
        print('a=b')


if __name__ == "__main__":
    main()
