#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
题目 统计 1 到 100 之和。

程序分析 无
"""


def main():
    sums = 0
    for i in range(1, 101):
        sums += i
    print(sum)


if __name__ == "__main__":
    main()
