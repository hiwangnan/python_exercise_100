#!/usr/bin/python3
# -*= coding:utf-8 -*-
"""
实例089：解码

题目 某个公司采用公用电话传递数据，数据是四位的整数，在传递过程中是加密的，加密规则如下：
每位数字都加上5,然后用和除以10的余数代替该数字，再将第一位和第四位交换，第二位和第三位交换。

程序分析 无。
"""


def main():
    data_str = input('请输入四个数字，用空格隔开(Q:退出)')
    while data_str not in 'qQ':
        data_list = data_str.split(' ')
        data = [int(n) for n in data_list if len(n) != 0]
        if len(data) != 4:
            print('输入有误！')
            data_str = input('请输入四个数字，用空格隔开(Q:退出)')
            continue
        print('显示值：', data)
        decoding_data = []
        for i in range(4):
            data[i] = (data[i] + 5) % 10
        for x in range(1, 5):
            decoding_data.append(data[-x])
        print('解密原始值：', decoding_data)
        data_str = input('请输入四个数字，用空格隔开(Q:退出)')


if __name__ == "__main__":
    main()
