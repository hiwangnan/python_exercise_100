#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例056：画圈

题目 画图，学用circle画圆形。

程序分析 无。
"""
from tkinter import *


def main():
    canvas = Canvas(width=800, height=600, bg='white')
    canvas.pack(expand=YES, fill=BOTH)
    k = 1
    j = 1
    for i in range(26):
        canvas.create_oval(310 - k, 250 - k, 310 + k, 250 + k, width=1)
        k += j
        j += 0.3
        print('正在进行。。。')
    mainloop()
    root = Tk()
    # 创建一个Canvas，设置其背景色为白色
    cv = Canvas(root, bg='white')
    # 创建一个矩形，坐标为(10,10,110,110)
    cv.create_rectangle(10, 10, 110, 110)
    cv.pack()
    root.mainloop()
    # 为明显起见，将背景色设置为白色，用以区别 root


if __name__ == "__main__":
    main()
