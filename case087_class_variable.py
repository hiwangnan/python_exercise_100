#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例087：访问类成员

题目 回答结果（结构体变量传递）。

程序分析 无。
"""


class Sutdent1(object):
    x = 0
    y = 0


def f(stu):
    stu.x = 20
    stu.c = 'c'


def main():
    a = Sutdent1()
    print(a.x, a.y)
    print(Sutdent1.x, Sutdent1.y)
    # print(a.c)   # 报错，实例没有该属性
    # 给实例a 添加x属性和c属性 并赋值
    a.x = 3
    a.c = 'a'
    print(a.x, a.y, a.c)
    print(Sutdent1.x, Sutdent1.y)  # 类的属性并没有改变
    # print(Sutdent1.c)  # 类没有C属性
    f(a)
    print(a.x, a.c)
    print(Sutdent1.x, Sutdent1.y)
    print(dir(a))  # 'c', 'x', 'y'
    print(dir(Sutdent1))  # 'x', 'y'


if __name__ == '__main__':
    main()
