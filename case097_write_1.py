#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例097：磁盘写入

题目 从键盘输入一些字符，逐个把它们写到磁盘文件上，直到输入一个 # 为止。

程序分析 无。
"""
from sys import stdout


def main():
    filename = input('输入文件名:\n')
    with open(filename, "a") as fp:
        ch = input('输入字符串:\n')
        while ch != '#':
            fp.write('\n'+ch)
            stdout.write(ch+'\n')
            print('print>>', ch)
            ch = input('')


if __name__ == '__main__':
    main()
