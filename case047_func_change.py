#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例047：函数交换变量

题目 两个变量值用函数互换。

程序分析 无
"""


def exc(a, b):
    return (b, a)


def main():
    a = int(input('请输入第一个数字：'))
    b = int(input('请输入第二个数字：'))
    print('输入的数字为:(a=%d,b=%d)' % (a, b))
    a, b = exc(a, b)
    print('交换为:(a=%d,b=%d)' % (a, b))


if __name__ == "__main__":
    main()
