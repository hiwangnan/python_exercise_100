#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例006：斐波那契数列

题目 斐波那契数列。

程序分析 斐波那契数列（Fibonacci sequence），从1,1开始，后面每一项等于前面两项之和。图方便就递归实现，图性能就用循环。
"""


def gen():
    """
    数列生成器
    """
    x = 1
    yield x
    y = 1
    yield y
    while True:
        x = x + y
        yield x
        y = x + y
        yield y


def fib(n):
    """
    递归 可直接求出第数列的第n个数
    """
    return 1 if n <= 2 else fib(n-1)+fib(n-2)


def main_gen():
    lis = []
    g = gen()
    num = next(g)
    lis.append(num)
    nums = int(input('请输入数列个数:'))
    while len(lis) < nums:
        num = next(g)
        lis.append(num)
    print(lis)


def main_for():
    lis = [1, 1]
    nums = int(input('请输入数列个数:'))
    for i in range(2, nums):
        lis.append(lis[i-1]+lis[i-2])
    print(lis)


def main_fib():
    lis = []
    nums = int(input('请输入数列个数:'))
    for i in range(1, nums+1):
        lis.append(fib(i))
    print(lis)


if __name__ == '__main__':
    main_gen()
    main_for()
    main_fib()
