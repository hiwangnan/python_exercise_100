#!/usr/bin/python4
# -*- coding:utf-8 -*-
"""
实例084：连接字符串

题目 连接字符串。

程序分析 无。
"""


def main():
    delimiter = ','
    mylist = ['Brazil', 'Russia', 'India', 'China']
    print(delimiter.join(mylist))
    print('-'.join(mylist))
    print(' '.join(mylist))


if __name__ == "__main__":
    main()
