#! /udr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例037：排序

题目 对10个数进行排序。

程序分析 同实例005。
"""


def main():
    st_in = input('请输入数字，（用空格隔开）：')
    st_in_list = st_in.split(' ')
    int_in_list = [int(x) for x in st_in_list]
    print('输入的数字如下：', int_in_list)
    # for i in range(len(int_in_list)):
    #     for j in range(i, len(int_in_list)):
    #         if int_in_list[i] > int_in_list[j]:
    #             int_in_list[i], int_in_list[j] = int_in_list[j], int_in_list[i]
    lis = sorted(int_in_list)
    print(int_in_list)
    print(lis)
    int_in_list.sort()
    print(int_in_list)


if __name__ == "__main__":
    main()
