#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例077：遍历列表

题目 循环输出列表

程序分析 无。
"""

lis = ['moyu', 'niupi', 'xuecaibichi', 'shengfaji', '42']
for i in range(len(lis)):
    print(lis[i])
