#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例074：列表排序、连接

题目 列表排序及连接。

程序分析 排序可使用 sort() 方法，连接可以使用 + 号或 extend() 方法。
"""


def main():
    a = [2, 6, 8]
    b = [7, 0, 4]
    print(a + b)
    a.extend(b)
    print(a)
    c = sorted(a)
    print(c)
    a.sort()
    print(a)


if __name__ == "__main__":
    main()
