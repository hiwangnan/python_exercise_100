#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例039：有序列表插入元素

题目 有一个已经排好序的数组。现输入一个数，要求按原来的规律将它插入数组中。

程序分析 首先判断此数是否大于最后一个数，然后再考虑插入中间的数的情况，插入后此元素之后的数，依次后移一个位置。
"""


def main():
    lis = [1, 10, 100, 1000, 10000, 100000]
    n = int(input('请输入一个数'))
    for i in range(len(lis)):
        if lis[i] >= n:
            lis[i:i] = [n]  # 在list中i位置插入元素，插入的必须是iterable， 字符串，list，tuple等可迭代
            break
    # for i in range(len(lis)):
    #     if lis[i] >= n:
    #         lis = lis[0:i] + [n] + lis[i:]
    #         break
    print(lis)


if __name__ == "__main__":
    main()
