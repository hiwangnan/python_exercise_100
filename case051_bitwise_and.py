#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例051：按位与

题目 学习使用按位与 & 。

程序分析 0&0=0; 0&1=0; 1&0=0; 1&1=1。 有一个0 即为0
"""


def main():
    a = 0o77
    print(a)
    b = a & 3
    print(b)
    b = b & 7
    print(b)


if __name__ == "__main__":
    main()
