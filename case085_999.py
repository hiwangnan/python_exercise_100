#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例085：整除

题目 输入一个奇数，然后判断最少几个 9 除于该数的结果为整数。

程序分析 999999 / 13 = 76923。
"""


def divide_9(n):
    M = 9
    num = M / n  # 除的结果
    m = 1  # 9的个数
    # x = M / n  # 除的结果
    while True:
        if M % n == 0:
            num = int(M / n)
            break
        M = M * 10 + 9
        m += 1
        if m % 2 == 0:
            print('正在计算。。。')
    return m, num


def main():
    n = input('请输入一个奇数：')
    while n not in 'Qq':
        if int(n) % 2 != 0 and int(n) % 5 != 0:
            m, num = divide_9(int(n))
            print('最少%d个9除以%d的结果为整数：%d' % (m, int(n), num))
            n = input('请输入一个奇数：')
        else:
            print('输入有误')
            n = input('请输入一个奇数：')


if __name__ == "__main__":
    main()
