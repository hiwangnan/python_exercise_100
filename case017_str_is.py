#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例017：字符串构成

题目 输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。

程序分析 利用 while 或 for 语句,条件为输入的字符不为 ‘\n’。
"""


def get_nums(string):
    alp = 0
    num = 0
    spa = 0
    oth = 0
    for i in range(len(string)):
        if string[i].isspace():         # 如果字符串中只包含空格，则返回 True，否则返回 False.
            spa += 1
        elif string[i].isdigit():       # 如果字符串只包含数字则返回 True 否则返回 False。
            num += 1
        elif string[i].isalpha():       # 如果字符串至少有一个字符并且所有字符都是字母则返回 True，否则返回 False。
            alp += 1
        else:
            oth += 1
    return alp, num, spa, oth


def main():
    string = input("输入字符串：")
    print('\"%s\"中字符统计：' % string)
    alp, num, spa, oth = get_nums(string)
    print('space: %d \ndigit: %d\nalpha: %d\nother: %d' % (spa, num, alp, oth))


if __name__ == "__main__":
    main()
