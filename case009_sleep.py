#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例009：暂停一秒输出

题目 暂停一秒输出。

程序分析 使用 time 模块的 sleep() 函数。
"""
import time


def main():
    print('Loading', end='')
    for i in range(10):
        print('.', end='')
        time.sleep(1)
    print('\n')
    # time.time() 回当前时间的时间戳（1970纪元后经过的浮点秒数）,是float类型，单位是s
    # print(time.time(), type(time.time()))
    for i in range(4):
        print(str(int(time.time()))[-2:])
        time.sleep(1)


if __name__ == '__main__':
    main()
