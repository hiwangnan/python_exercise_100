#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例078：字典

题目 找到年龄最大的人，并输出。请找出程序中有什么问题。

程序分析 无。
"""


def main_dict():
    person = {"li": 18, "wang": 50, "zhang": 20, "sun": 22}
    # m = 'li'
    # for key in person.keys():
    #     if person[m] < person[key]:
    #         m = key
    # print ('%s,%d' % (m,person[m]))
    age_list = list(person.values())
    mix_age = age_list[0]
    for age in age_list:
        if mix_age < age:
            mix_age = age
    mix_age_d = dict()
    for key, age in person.items():
        if age == mix_age:
            mix_age_d[key] = age
    print(mix_age_d)


if __name__ == '__main__':
    main_dict()
