#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例067：交换位置

题目 输入数组，最大的与第一个元素交换，最小的与最后一个元素交换，输出数组。

程序分析 找出最大值和最小值，并记录index，将首和末尾与最大值最小值交换。
"""


def main():
    li = [3, 2, 5, 7, 8, 1, 5]
    max_index = 0
    max_num = li[0]
    min_index = 0
    min_num = li[0]
    for i in range(len(li)):
        if li[i] > max_num:
            max_num = li[i]
            max_index = i
        if li[i] < min_num:
            min_num = li[i]
            min_index = i
    # print(max_num, min_num)
    li[max_index] = li[0]
    li[0] = max_num
    li[min_index] = li[-1]
    li[-1] = min_num
    print(li)


if __name__ == "__main__":
    main()
