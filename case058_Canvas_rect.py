#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例058：画矩形

题目 画图，学用rectangle画方形。

程序分析 无。
"""
from tkinter import *


def main():
    root = Tk()
    root.title('画一个矩形')
    root.geometry('400x400')
    # root.resizable(300, 400)

    def creat_rec():
        cv = Canvas(root, width=300, height=400,)
        cv.create_rectangle(10, 10, 210, 210, fill='red', outline='blue', width=5, tags='r1')
        cv.tag_bind('r1', '<Button-1>', printRect)
        cv.pack()

    def printRect(event):
        print('rectangle')

    frame = Frame(root)
    # frame.grid(row=1, column=2)
    frame.pack()
    # but = Button(frame, text='画矩形', command=creat_rec)
    # # grid组件使用行列的方法放置组件的位置，参数有：
    # # column:         组件所在的列起始位置；
    # # columnspam:     组件的列宽；
    # # row：      　　　组件所在的行起始位置；
    # # rowspam：    　　组件的行宽；
    # but.grid(row=1, column=1)
    # # place组件可以直接使用坐标来放置组件，参数有：
    # # anchor:    　　　组件对齐方式；
    # # x:        　　　 组件左上角的x坐标；
    # # y:        　　   组件右上角的y坐标；
    # # relx:         　组件相对于窗口的x坐标，应为0-1之间的小数；
    # # rely:           组件相对于窗口的y坐标，应为0-1之间的小数；
    # # width:          组件的宽度；
    # # heitht:    　   组件的高度；
    # # relwidth:       组件相对于窗口的宽度，0-1；
    # # relheight:　    组件相对于窗口的高度，0-1；
    # # but.place(relx=0.2, rely=0.2)
    root.mainloop()


if __name__ == "__main__":
    main()
