#! /usr/bin/python3
# _*_ conding:utf-8 _*_
"""
实例052：按位或

题目 学习使用按位或 | 。

程序分析 0|0=0; 0|1=1; 1|0=1; 1|1=1  有1即1 双0是0 双1是1
"""


def main():
    a = 0o77
    print(a)
    print(a | 3)
    print(a | 3 | 7)
    print(int(str(a), base=8))


if __name__ == "__main__":
    main()
