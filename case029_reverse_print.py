#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
    实例029：反向输出

    题目 给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字。

    程序分析 学会分解出每一位数,用字符串的方法总是比较省事。
"""


def main():
    str_in = input('请输入整数：')
    le = len(str_in)
    print('几位数：', le)
    print(str_in[::-1])
    for i in range(1, le + 1):
        print(str_in[-i], end=' ')
    print()


if __name__ == "__main__":
    main()
