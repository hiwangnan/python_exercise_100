#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例096：计算复读次数

题目 计算字符串中子串出现的次数。
count() 计算字符串在另外一个字符串里出现的次数
程序分析 无。
"""
s1 = 'xuebixuebxuebixuebixuebixuebixuebixue'
s2 = 'xuebi'
print(s1.count(s2))
