#!/usr/bin/python3
# -*-coding:utf-8 -*-
"""
实例093：time模块III

题目 时间函数举例3。

程序分析 如何浪费时间。

start = time.clock() # python3 time模块不适用clock函数了  用per_counter 代替
"""
import time
import functools


def log(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kw):
            start = time.perf_counter()
            print('函数：%s 开始运行' % text)
            print('开始时间是：%f' % start)
            f = func(*args, **kw)
            end = time.perf_counter()
            print('函数：%s 运行结束' % text)
            print('结束时间是：%f' % end)
            print('different is %6.4f' % (end - start))
            print('different is %.4f' % (end - start))
            return f

        return wrapper

    return decorator


@log('ran')
def ran(n):
    time.sleep(2)
    for i in range(n):
        print(time.perf_counter())


def per():
    time.sleep(1)
    a = time.perf_counter()  # 第一次调用per_counter,
    print(a)
    print(round(a))  # 把a四舍五入验证下
    print(type(a))  # 验证a是浮点数
    time.sleep(5)
    b = time.perf_counter()
    print(b)


def main():
    a = time.perf_counter()
    print(a)
    print(round(a))
    time.sleep(1)
    n = 60
    ran(n)
    per()


if __name__ == '__main__':
    main()
