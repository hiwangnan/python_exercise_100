#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例031：字母识词

题目 请输入星期几的第一个字母来判断一下是星期几，如果第一个字母一样，则继续判断第二个字母。

程序分析 这里用字典的形式直接将对照关系存好。
"""


def main():
    week_list = [
        'monday', 'tuesday', 'wensday', 'thursday', 'friday', 'saturday',
        'sunday'
    ]
    week_t = {'h': 'thursday', 'u': 'tuesday'}
    week_s = {'a': 'saturday', 'u': 'sunday'}
    week = {
        't': week_t,
        's': week_s,
        'm': 'monday',
        'w': 'wensday',
        'f': 'friday'
    }
    fist_a = input('请输入第一个字母').lower()
    if fist_a == 't':
        sen_a = input('请输入第二个字母').lower()
        print(week_t[sen_a])
    elif fist_a == 's':
        sen_a = input('请输入第二个字母').lower()
        print(week_s[sen_a])
    elif fist_a in 'mwf':
        print(week[fist_a])
    else:
        print('输入有误！')
    if fist_a in 'ts':
        fist_a += input('请输入第二个字母').lower()
        for i in week_list:
            if i[:2] == fist_a:
                print(i)
    elif fist_a in 'mwf':
        for i in week_list:
            if i[0] == fist_a:
                print(i)
    else:
        print('输入有误！')


if __name__ == "__main__":
    main()
