#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例082：八进制转十进制

题目 八进制转换为十进制

程序分析 无。
"""


def main():
    x_0o = input('请输入8进制的数：')
    while True:
        try:
            x_0o_int = int(x_0o, base=8)
            if x_0o[0] == '0':
                print(x_0o, '的十进制是：', x_0o_int)
            else:
                print('0o' + x_0o, '的十进制是：', x_0o_int)
            break
        except ValueError:
            print('输入有误')
            x_0o = input('请输入8进制的数：')


if __name__ == "__main__":
    main()
