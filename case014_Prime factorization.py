#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例014：分解质因数

题目 将一个整数分解质因数。例如：输入90,打印出90=2×3×3×5。

程序分析 根本不需要判断是否是质数，从2开始向数本身遍历，能整除的肯定是最小的质数。
"""


def main():
    n = int(input('请输入一个的整数'))
    print('%d = ' % n, end='')
    if n < 0:
        n = abs(n)
        print('-1*', end='')
    if n <= 1:
        print(n)
    else:
        num_list = []
        while True:
            for i in range(2, n+1):
                if n % i == 0:
                    print('%d' % i, end='')
                    num_list.append(i)
                    if i != n:
                        print('*', end='')
                        is_prime = False
                        n = int(n / i)
                    else:
                        is_prime = True
                    break
            if is_prime:
                break
        print()
        # print(num_list)


if __name__ == "__main__":
    main()
