#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例002：“个税计算”

题目 企业发放的奖金根据利润提成。
利润(I)低于或等于10万元时，奖金可提10%；
利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可提成7.5%；
20万到40万之间时，高于20万元的部分，可提成5%；
40万到60万之间时高于40万元的部分，可提成3%；
60万到100万之间时，高于60万元的部分，可提成1.5%，
高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？

程序分析 分区间计算即可。
————————————————
版权声明：本文为CSDN博主「超级大黄狗Shawn」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/weixin_41084236/article/details/81564963
"""


def main():
    i = int(input('请输入当月的利润I(万元):'))
    if i <= 10:
        cash = i * 0.1
    elif 10 < i < 20:
        cash = (i - 10) * 0.075 + 10 * 0.1
    elif 20 <= i <= 40:
        cash = (i - 20) * 0.05 + 10 * 0.075 + 10 * 0.1
    elif 40 <= i <= 60:
        cash = (i - 40) * 0.05 + 20 * 0.05 + 10 * 0.075 + 10 * 0.1
    elif 60 <= i <= 100:
        cash = (i - 60) * 0.015 + 20 * 0.03 + 20 * 0.05 + 10 * 0.075 + 10 * 0.1
    elif 100 <= i:
        cash = (
            i - 100
        ) * 0.01 + 40 * 0.015 + 20 * 0.03 + 20 * 0.05 + 10 * 0.075 + 10 * 0.1
    else:
        print('输入有误')
    print('可得奖金（万元）', cash)


def main_p():
    profit = int(input('Show me the money: '))
    bonus = 0
    thresholds = [100000, 100000, 200000, 200000, 400000]
    rates = [0.1, 0.075, 0.05, 0.03, 0.015, 0.01]
    for i in range(len(thresholds)):
        if profit <= thresholds[i]:
            bonus += profit * rates[i]
            profit = 0
            break
        else:
            bonus += thresholds[i] * rates[i]
            profit -= thresholds[i]
    bonus += profit * rates[-1]
    print(bonus)


if __name__ == '__main__':
    main()
    main_p()
