#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例049：lambda

题目 使用lambda来创建匿名函数。

程序分析 无
"""


def main():
    a = int(input('请输入第一个数字：'))
    b = int(input('请输入第二个数字：'))
    print('输入的数字为:(a=%d,b=%d)' % (a, b))
    print((lambda x, y: x * (x >= y) + y * (y > x))(a, b))
    print((lambda x, y: x * (x <= y) + y * (y < x))(a, b))


if __name__ == "__main__":
    main()
